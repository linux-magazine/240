# Mr. Wibbly Wobbly - An Extension for Inkscape

Full article of how this came about and what you can use it for in [Linux Magazine #240](https://www.linux-magazine.com/Issues/2020/240).


## Converting Frames

Many video editors will not know how to manage a sequence of SVG images (laaaaaaaaaaame!) so you will have to convert your SVG frames to PNGs first. You can do that automatically using ImageMagick's *convert* tool.

Cd into the directory where your frames are stored and do this:

```
for i in *.svg; do convert -background transparent $i ${i%svg}png; rm $i; done
```

You can then use the image sequence in something like ~~Kdenlive~~ (**NEWS FLASH:** Kdenlive now supports importing sequences of SVG images. [Kdenlive](https://kdenlive.org) is awesome!) and overlay your wobbly animation on to something else.

You could also make a video using FFMpeg as follows:

```
ffmpeg -framerate 25 -i frame%03d.png wobblyoutput.mp4
```

But then you would lose the transparent background.

## Fluff

[Here's a video that explains where I got the name for this plugin](https://libre.video/videos/watch/b912b05f-aeb7-4649-a60d-cf69505a180b).
