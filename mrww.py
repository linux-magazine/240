#!/usr/bin/env python
# coding=utf-8

import random
import inkex
from inkex import command, styles

class Mrww (inkex.EffectExtension):
    
    def add_arguments (self, pars):
        pars.add_argument ("--radius", type=float, default=1, help="Radius")
        pars.add_argument ("--frames", type=int, default=1, help="Frames")
        pars.add_argument ("--folder", type=str, help="Path")
        pars.add_argument ("--accumulate", type=inkex.Boolean, help="Accumulate deformation")
    
    def effect(self):
        accumulate = self.options.accumulate
        for node in self.svg.get_selected(inkex.PathElement):

            path = node.path.to_superpath()
            o_node=node.duplicate()
            o_node.set("opacity", "0.0")
            
            for frame in range (0, self.options.frames):
                if not(accumulate):
                    path = o_node.path.to_superpath()
                for subpath in path:
                    closed = subpath[0] == subpath[-1]
                    for index, csp in enumerate(subpath):
                        if closed and index == len(subpath) - 1:
                            subpath[index] = subpath[0]
                            break
                        else:
                            delta = random.uniform (-self.options.radius, self.options.radius)
                            csp[0][0] += delta
                            csp[0][1] += delta
                            csp[1][0] += delta
                            csp[1][1] += delta
                            csp[2][0] += delta
                            csp[2][1] += delta
        
                node.path = path
                command.write_svg(self.svg, self.options.folder + "/frame" + f'{frame:03}' + ".svg")

if __name__ == '__main__':
    Mrww().run()
